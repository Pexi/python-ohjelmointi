carInfo = {
    "brand": "Toyota",
    "model": "Avensis",
    "year":  2010,
    "price": 12499.95

}

print(carInfo)

#Jos dictionary ei sisällä avainta,lisätään avain-arvopari dictionaryyn
carInfo["color"] = "red"
print(carInfo)

# Jos avain on olemassa, arvo päivitetään
carInfo["year"] = 2011
print(carInfo)

#Dictionaryn arvoja voidaan indeksoida avaimien avulla
price = carInfo["price"]
print("Hinta", price)

# pop poistaa avainta vastaavan avainarvo parin jos sellainen löytyy
prince =carInfo.pop("price")
print("Poistettu hinta", price)

price = carInfo.pop("price", 0)
print("Poistettu hinta", price)

print(carInfo)

carInfo2 = {
    "brand": "Jeep",
    "model": "Compass",
    "year":  2010,
    "price": 12499.95,
    "color": "green"

}

carList =[carInfo, carInfo2]
print("")
print("")
print(carList, sep="\n")