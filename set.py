units = {"tank", "plane", "ship", "dog"}
print(units)
# Lisää alkion settiin
units.add("soldier")
print(units)
# alkioita settiin Lisää listallinen
units.update(["heavy tank", "helicopter"])
print(units)

# Discard poistaa alkion. Jos alkiota ei ole setissä,
# discard ei aiheuta virhettä
units.discard("dog")
units.discard("dog")
print(units)


#Remove  poistaa alkion ja heittää virheen, jos alkiota ei löytdy setistä
units.remove("soldier")
#units.remove("dog")
