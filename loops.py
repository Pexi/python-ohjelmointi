a = 10

while a >= 0:
    print(a)
    a -= 1 # vähennetään a:n arvoa yhdellä
    # a = a-1  #sama kuin yllä
    # # a++   #Tämä ei toimi pythonissa
    # a--     # Eikä tämäkään
#else:
    #print"Silmukka

print("A:n arvo sovelluksen lopussa", a)

cars = ["Volvo", "Toyota", "Fiat", "Audi", "Seat"]

for car in cars:
    print("Auto: ", car)

lenght = len(cars)
for index in range(0, lenght):
    car = cars[index]
    print("Auto:", car)
