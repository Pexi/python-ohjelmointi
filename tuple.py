phones = ("iPhone", "Samsung Galaxy", "Nokia")
print(phones)

phone = phones[1] # Palauttaa alkion indeksissä
print(phone)

last = phones[-1] # Palauttaa viimeisen alkion listasta
print("Viimeinen:", last)

phoneList = list(phones)
phoneList.append("One plus")
phones = tuple(phoneList) #Tekee listasta tuplan ja sijoittaa sen alkuperäisen
# tuplan sisälle
print(phones)