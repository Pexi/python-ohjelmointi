a = 0 # yksi = sijoittaa muuttujan tiedostoon
b = 1
c = a == b

print(c) # Testataan, onko a yhtäsuuri kuin b ja tallennetaa tulos muuttujaan c

if a == b:
    print("a is equal to b")
    print("This belongs to the first if too")
else:
    print("a is not equal to b")