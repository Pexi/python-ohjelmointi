greeting = "Hello , World !"
print(greeting)
print("Hello World")
print("How are you ?")
print('\'How are you ?\', someone said ')

sum = greeting + str(1)
print(sum)
# Muuta käyttäjän nimi siten, että nimen ensimmäinen kirjain on
# kirjoitettu isolla ennen nimen tulostamista
name = input("What is your name? >  ")
name = name.capitalize()
# Onko name-muutujassa vähintään 1 kirjain ja onko kirjain
# kirjoitettu pienellä
if len(name) > 0 and name[0].islower:
# koodiblocki merkaytaan sisennyksellä
# esim if-lauseen sisältö pitää kirjoittaa sisennettynä
   originalName = name
   name = name[0].upper() 
   name += originalName[1:]
   # name = name + originalName[1:]

longText ="Moroooshrhorhohroherohgo.jeroijjrjjpjrwp \
.oenrnerernriuerireibii,oirgogrrgoero. \
 nrinonoernone"

print("Hi " + name)

