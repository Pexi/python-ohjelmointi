# Phytonissa muuttujille ei tarvitse määrittää tyyppiä,
# muuttujilla silti on tyyppi, alla integer
int1 = 2
int2 = 5

result = int1 - int2
print(result)

result = int1 * int2
print(result)

result = int1 / int2
print(result)

# kahden intergerin välinen jakolasku palauttaa Pythonissa
# floatin point arvon (desimaali luvun) jos tulos halutaan
# intergerinä ,pitää käyttää operaattoria
result //= int2 // int1
print(result)

float1 = 2.5
float2 = 1.5
sum = float1 + float2
print(sum)

division = float1 // float2
print(division)

bool1 = True
bool2 = False
int1 = 1
if bool1 or bool2:
    print("Tosi")
else:
    print("Epätosi")