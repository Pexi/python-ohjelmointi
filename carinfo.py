carInfo = ["Volvo", 2019, 25200, 95] 
carList = ["Toyota", "Volvo", "Mini"]
print(carInfo)
print(carList)
car = carList[2]
print(car)

carList.append("Jeep") # Lisää alkion listan loppuun
carList.insert(2, "Fiat") #Lisää alkion tiettyyn indeksiin
print(carList)

removed = carList.pop() #pop-funktio poistaa viimeisen alkion listasta ja
# palauttaa sen paluuarvon
print("Removed car", removed, sep="-" )
print(carList)
del carList[1] # del poistaa listan alkiot tietyistä indekseistä
print(carList)
# del carlist # del poistaa koko listan ,jos indeksillä ei ole määritelty
#print(carList) # Tältä riviltä tulee Name Error virhe, koska lista on poistettu

carList.remove("Toyota")
print(carList)
